
import React from 'react';
import Bird from './Bird';

function App() {
  return (
  <div className="App" style={{backgroundColor: 'white'}}>
  <Bird />
  </div>
);
}
export default App;

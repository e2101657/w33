
import React from 'react';
import birdData from './bird.json';
function Bird() {
    const sortedBirdData = birdData.sort((a, b) => {
     return a.Finnish.localeCompare(b.Finnish);
      });   
  return (
    <div className="Bird">
    <table style={{ width: '100%', textAlign: 'left'}}>
    <thead style={{ backgroundColor: 'lightgray'}}>
        <tr>
        <th style={{ fontWeight: 'bold' }}>Finnish</th>
        <th style={{ fontWeight: 'bold' }}>Swedish</th>
        <th style={{ fontWeight: 'bold' }}>English</th>
        <th style={{ fontWeight: 'bold' }}>Short</th>
        <th style={{ fontWeight: 'bold' }}>Latin</th>
        </tr>
    </thead>
    <tbody>
          {birdData.map((bird, index)=>(
            <tr key={index}>
            <td>{bird.Finnish}</td>
            <td>{bird.Swedish}</td>
            <td>{bird.English}</td>
            <td>{bird.Short}</td>
            <td>{bird.Latin}</td>
     </tr>
        ))}
    </tbody>
    </table>
</div>
);
}
export default Bird;
